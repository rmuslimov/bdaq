(ns bdaq.settings
  (:require [config.core :refer [env]]))

;;; -- Cider repl
(def cider-port (or (:cider-port env) 45267))
(def cider-host (or (:cider-host env) "0.0.0.0"))
