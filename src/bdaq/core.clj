(ns bdaq.core
  (:gen-class)
  (:require [bdaq
             [cider-nrepl :refer [new-cider-repl-server]]
             [settings :as ss]]
            [clojure.tools.logging :as log]
            [com.stuartsierra.component :as component]
            reloaded.repl
            )
  (:import com.globalbettingexchange.externalapi.SecureService_Service
           com.globalbettingexchange.externalapi.GetAccountBalancesRequest
           com.globalbettingexchange.externalapi.ExternalApiHeader
           ))

(defn prod-system
  "Production systems to start."
  []
  (component/system-map
   :cider (new-cider-repl-server ss/cider-port ss/cider-host)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (reloaded.repl/set-init! #'prod-system)
  (reloaded.repl/go)
  (log/info "System has been started at:" ss/cider-host ss/cider-port))

(comment
  (def service
    (SecureService_Service.))

  (def srv
    (. service getSecureService))

  (def gabr
    (GetAccountBalancesRequest.))

  (.getMethods srv)
  (.
   srv
   setHeader
   eheader)

  (def eheader
    (doto (ExternalApiHeader.)
      (.setVersion (new BigDecimal "2"))
      (.setLanguageCode "en")
      ))

  (def rsp
    (. srv getAccountBalances gabr eheader))

  (bean rsp)
  (bean
   (.getReturnStatus rsp))

)
