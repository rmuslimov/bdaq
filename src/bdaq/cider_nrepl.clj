(ns bdaq.cider-nrepl
  (:require [cider.nrepl :refer [cider-nrepl-handler]]
            [clojure.tools.nrepl.server :refer [start-server stop-server]]
            [com.stuartsierra.component :as component]
            [suspendable.core :as suspendable]
            [clojure.tools.logging :as log]))

(defrecord CiderReplServer [server port bind]
  component/Lifecycle
  (start [component]
    (log/info "Cider server starting at:" bind port)
    (assoc component
           :server (start-server
                    :port port
                    :handler cider-nrepl-handler
                    :bind bind)))
  (stop [{server :server :as component}]
    (when server
      (stop-server server))
    (dissoc component :server))

  suspendable/Suspendable
  (suspend [component]
    component)
  (resume [component old-component]
    (assoc component
           :server (:server old-component))))

(defn new-cider-repl-server
  ([port]
   (new-cider-repl-server port "localhost"))
  ([port bind]
   (map->CiderReplServer {:port port :bind bind})))
