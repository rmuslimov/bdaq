(defproject bdaq "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0-alpha15"]
                 [cider/cider-nrepl "0.15.0"]
                 [com.stuartsierra/component "0.3.2"]
                 [org.clojure/tools.nrepl "0.2.13"]
                 [reloaded.repl "0.2.3"]
                 [javax.ws.rs/javax.ws.rs-api "2.0"]
                 [yogthos/config "0.9"]]
  :main ^:skip-aot bdaq.core
  :target-path "target/%s"
  :resource-paths ["resources/gbe.jar"]
  :profiles {:uberjar {:aot :all}})
