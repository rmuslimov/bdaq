(ns user
  (:require [bdaq
             [cider-nrepl :refer [new-cider-repl-server]]
             [settings :as ss]]
            [com.stuartsierra.component :as component]
            reloaded.repl))

(defn dev-system
  "Production systems to start."
  []
  (component/system-map
   :cider (new-cider-repl-server ss/cider-port ss/cider-host)))

(reloaded.repl/set-init! #'dev-system)
